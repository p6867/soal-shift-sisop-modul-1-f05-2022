#!/bin/bash

access=$(date "+%m/%d/%y %H:%M:%S")

read -p "Username : " username
read -s -p "Password : " pass

if [ $(grep -w $username user.txt) ]
then
	echo -e "\n$access REGISTER:ERROR User already exist" >> log.txt
	echo -e "\n$access REGISTER:ERROR User already exist"
	exit
fi

pass_length=${#pass}
#echo $pass_length

if [ $pass_length -lt 8 ]
then
	echo -e  "\n$access REGISTER:ERROR Password less than 8 characters" >> log.txt
	echo -e "\n$access REGISTER:ERROR Password less than 8 characters"
	exit
fi

if  [[ ! $pass =~ [A-Z] ]] || [[ ! $pass =~ [a-z] ]]
then
	echo -e "\n$access REGISTER:ERROR Password must consist of uppercase and lowercase" >> log.txt
	echo -e "\n$access REGISTER:ERROR Password must consist of uppercase ande lowercase"
	exit
fi

if [ $pass =~ [^a-zA-Z0-9] ]
then
	echo -e "\n$access REGISTER:ERROR Password must to be Alphanumeric" >> log.txt
	echo -e "\n$access REGISTER:ERROR Password must to be Alphanumeric"
	exit
fi

#problem : selalu menghasilkan username=pass
if [ "$username" = "$pass" ]
then
        echo -e "\n$access REGISTER:ERROR Password can't be the same as Username" >> log.txt
        echo -e "\n$access REGISTER:ERROR Password can't be the same as Username"
#        echo -e "\n${#pass} tdk sm dgn ${#username}"
        exit
fi

echo -e "\nUsername : $username \nPassword : $pass" >> user.txt
echo -e "\n$access REGISTER:INFO User $username registered successfully" >> log.txt
echo -e "\n$access REGISTER:INFO User $username registered successfully"
