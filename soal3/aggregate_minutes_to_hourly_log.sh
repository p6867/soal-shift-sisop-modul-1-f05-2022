#!/bin/bash

avg_result=0

average(){
 local sum=0
 
 for i in echo $*
 do
   let sum=$sum+$i
 done
 
 avg_result=`expr $sum / $#`
}

first="metrics_agg_"
date=`date +%Y%m%d%H`
last=".log"

#mengambil data

path="/home/azzura13/"

mem_total=`awk -F "," '/home/ {print $1}' /home/azzura13/log/metrics_$date*`
mem_used=`awk -F "," '/home/ {print $2}' /home/azzura13/log/metrics_$date*`
mem_free=`awk -F "," '/home/ {print $3}' /home/azzura13/log/metrics_$date*`
mem_shared=`awk -F "," '/home/ {print $4}' /home/azzura13/log/metrics_$date*`
mem_buff=`awk -F "," '/home/ {print $5}' /home/azzura13/log/metrics_$date*`
mem_available=`awk -F "," '/home/ {print $6}' /home/azzura13/log/metrics_$date*`

swap_total=`awk -F "," '/home/ {print $7}' /home/azzura13/log/metrics_$date*`
swap_used=`awk -F "," '/home/ {print $8}' /home/azzura13/log/metrics_$date*`
swap_free=`awk -F "," '/home/ {print $9}' /home/azzura13/log/metrics_$date*`

path_size=`awk -F "," '/home/ {print $11}' /home/azzura13/log/metrics_$date*`

#memproses data

#minimum

mem_total_min=`echo $mem_total | tr " " "\n" | sort -g | head -1`
mem_used_min=`echo $mem_used | tr " " "\n" | sort -g | head -1`
mem_free_min=`echo $mem_free | tr " " "\n" | sort -g | head -1`
mem_shared_min=`echo $mem_shared | tr " " "\n" | sort -g | head -1`
mem_buff_min=`echo $mem_buff | tr " " "\n" | sort -g | head -1`
mem_available_min=`echo $mem_available | tr " " "\n" | sort -g | head -1`

swap_total_min=`echo $swap_total | tr " " "\n" | sort -g | head -1`
swap_used_min=`echo $swap_used | tr " " "\n" | sort -g | head -1`
swap_free_min=`echo $swap_free | tr " " "\n" | sort -g | head -1`

path_size_min=`echo $path_size | tr " " "\n" | sort -g | head -1`

#maximum

mem_total_max=`echo $mem_total | tr " " "\n" | sort -g | tail -1`
mem_used_max=`echo $mem_used | tr " " "\n" | sort -g | tail -1`
mem_free_max=`echo $mem_free | tr " " "\n" | sort -g | tail -1`
mem_shared_max=`echo $mem_shared | tr " " "\n" | sort -g | tail -1`
mem_buff_max=`echo $mem_buff | tr " " "\n" | sort -g | tail -1`
mem_available_max=`echo $mem_available | tr " " "\n" | sort -g | tail -1`

swap_total_max=`echo $swap_total | tr " " "\n" | sort -g | tail -1`
swap_used_max=`echo $swap_used | tr " " "\n" | sort -g | tail -1`
swap_free_max=`echo $swap_free | tr " " "\n" | sort -g | tail -1`

path_size_max=`echo $path_size | tr " " "\n" | sort -g | tail -1`

#average

average $mem_total
mem_total_avg=$avg_result
average $mem_used
mem_used_avg=$avg_result
average $mem_free
mem_free_avg=$avg_result
average $mem_shared
mem_shared_avg=$avg_result
average $mem_buff
mem_buff_avg=$avg_result
average $mem_available
mem_available_avg=$avg_result

average $swap_total
swap_total_avg=$avg_result
average $swap_used
swap_used_avg=$avg_result
average $swap_free
swap_free_avg=$avg_result

path_size_avg=`echo $path_size | tr " " "\n" | head -1`

#cetak file log
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/azzura13/log/$first$date$last
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$path,$path_size_min" >> /home/azzura13/log/$first$date$last
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$path,$path_size_max" >> /home/azzura13/log/$first$date$last
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$path,$path_size_avg" >> /home/azzura13/log/$first$date$last

echo `chmod 400 /home/azzura13/log/$first$date$last`