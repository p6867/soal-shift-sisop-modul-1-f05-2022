#!/bin/bash

prefix="/home/azzura13/log/metrics_"
postfix=`date +%Y%m%d%H%M%S.log`

mem=`free -m | grep Mem: | awk '{print $2,$3,$4,$5,$6,$7}'`
mData=($mem)

swap=`free -m | grep Swap: | awk '{print $2,$3,$4}'`
sData=($swap)

path="/home/azzura13/"
pSize=`du -sh $path | awk '{print $1}'`

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $prefix$postfix
echo "${mData[0]},${mData[1]},${mData[2]},${mData[3]},${mData[4]},${mData[5]},${sData[0]},${sData[1]},${sData[2]},$path,$pSize" >> $prefix$postfix

echo `chmod 400 $prefix$postfix`