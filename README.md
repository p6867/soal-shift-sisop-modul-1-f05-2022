# Laporan Resmi Soal Shift Sistem Operasi Modul 1 F05 2022

Kelompok F05 :

                - Azzura Mahendra Putra Malinus     5025201211
                - Syaiful Bahri Dirgantara          5025201203
                - Wahyu Tri Saputro                 5025201217

## Soal 1

Soal meminta untuk membuat sistem resgister dan login dimana ditetukan beberapa syarat untuk username dan passwordnya. Untuk sistem register harus membuat file `register.sh`. Sedangkan untuk sistem login harus membuat file `main.sh`. Data user yang sudah berhasil terdaftar harus tersimpan di `./users/user.txt` dan semua aktivitas disimpan di `.users/log.txt`.

### Pengerjaan Soal

#### 1. Membuat file register.sh dan main.sh

```bash
touch main.sh
nano register.sh
```

#### 2. Membuat variabel untuk template waktu

```bash
access=$(date "+%m/%d/%y %H:%M:%S")
```

#### 3. Membaca input username dan password dari user

```bash
read -p "Username : " username
read -s -p "Password : " pass
```

#### 4. Membuat kondisi jika username sudah ada di dalam user.txt

```bash
if [ $(grep -w $username user.txt) ]
then
        echo -e "\n$access REGISTER:ERROR User already exist" >> log.txt
        echo -e "\n$access REGISTER:ERROR User already exist"
        exit
fi
```

#### 5. Mendefinisikan variabel yang berisi panjang password

```bash
pass_length=${#pass}
```

#### 6. Membuat kondisi jika password kurang dari 8 karakter

```bash
if [ $pass_length -lt 8 ]
then
        echo -e  "\n$access REGISTER:ERROR Password less than 8 characters" >> >
        echo -e "\n$access REGISTER:ERROR Password less than 8 characters"
        exit
fi
```

#### 7. Membuat kondisi jika password tidak berisi uppercase atau lowercase

```bash
if [[ ! $pass =~ [A-Z] ]] || [[ ! $pass =~ [a-z] ]]
then
        echo -e "\n$access REGISTER:ERROR Password must consist of uppercase an>
        echo -e "\n$access REGISTER:ERROR Password must consist of uppercase an>
        exit
fi
```

#### 8. Membuat kondisi jika password berisi karakter yang bukan alphanumeric

```bash
if [[ $pass =~ [^a-zA-Z0-9] ]]
then
        echo -e "\n$access REGISTER:ERROR Password must to be Alphanumeric" >> >
        echo -e "\n$access REGISTER:ERROR Password must to be Alphanumeric"
        exit
fi
```

#### 9. Membuat kondisi jika username dan password memiliki isi yang sama

```bash
if [ "$username" = "$pass" ]
then
        echo -e "\n$access REGISTER:ERROR Password can't be the same as Usernam>
        echo -e "\n$access REGISTER:ERROR Password can't be the same as Usernam>
        exit
fi
```

#### 10. Membuat perintah jika username dan password memenuhi syarat

```bash
echo -e "\nUsername : $username \nPassword : $pass" >> user.txt
echo -e "\n$access REGISTER:INFO User $username registered successfully" >> log>
echo -e "\n$access REGISTER:INFO User $username registered successfully"
```

### Kendala dalam Pengerjaan

Mengalami stuck lama pada saat pengerjaan file `register.sh` yaitu di bagian username sama dengan password. Tidak menambahkan spasi diantara tanda `=` sehingga output yang dihasilkan selalu tidak sesuai yang diharapkan. Karena stuck pada pengerjaan file `resgister.sh` maka file `main.sh` tidak sempat dikerjakan hingga selesai. Beberapa syarat untuk password awalnya tidak berfungsi dengan baik, namun akhirnya sudah berfungsi setelah diperbaiki.

### Dokumentasi

#### 1. Username sudah ada di user.txt

![dokum_1.1](Dokumentasi/Soal1/Dokum_modul1_user_already_exist.png)

#### 2. Password kurang dari 8 karakter

![dokum_1.2](Dokumentasi/Soal1/Dokum_modul1_lt_8_char.png)

#### 3. Password tidak berisi uppercase dan lowercase

![dokum_1.3](Dokumentasi/Soal1/Dokum_modul1_not_upper_and_lowercase.png)

#### 4. Password tidak alphanumeric

![dokum_1.4](Dokumentasi/Soal1/Dokum_modul1_not_alphanumeric.png)

#### 5. Password sama dengan username

![dokum_1.5](Dokumentasi/Soal1/Dokum_modul1_pass_same_as_uname.png)

#### 6. Registrasi berhasil

![dokum_1.6](Dokumentasi/Soal1/Dokum_modul1_regis_succes.png)

#### 7. Isi file user.txt

![dokum_1.7](Dokumentasi/Soal1/user.txt.png)

#### 8. Isi file log.txt

![dokum_1.8](Dokumentasi/Soal1/log.txt.png)

## Soal 2
Di dalam soal 2, kita diminta untuk membuat script awk untuk membantu memproses sebuah log yang diberikan soal. Kemudian membuat dua buah file txt yaitu ratarata.txt untuk melihat hasil request per jam dan result.txt untuk melihat IP yang paling banyak mengakses beserta waktu aksesnya.

### Pengerjaan Soal

#### 1. Membuat folder forensic_log_website_daffaindo_log
```bash
mkdir -p forensic_log_website_daffaindo_log
```

#### 2. Membuat variabel bernama filerata2 dan fileresult
```bash
filerata2="forensic_log_website_daffaindo_log/ratarata.txt"
fileresult="forensic_log_website_daffaindo_log/result.txt"
```
Variabel tersebut digunakan untuk menyimpan hasil script ke dalam dua file txt.

#### 3. Menampilkan request per jam
```bash
cat log_website_daffainfo.log | awk -F':"' '{print $2}' | awk -F"/" '{print $1,$3}' | awk -F":" '{print $1,$2,$3}' | awk -F" " '{print $1,$3}' > temp_date_time
```
Script di atas adalah untuk menampilkan waktu request per jam dari file log_website_daffainfo.log, kemudian hasil output nya dimasukkan ke sebuah file temporary yaitu temp_date_time.

```bash
cat temp_date_time | awk -F':"' '{print $2}' | awk -F"/" '{print $1,$3}' | awk -F":" '{print $1,$2,$3}' | awk -F" " '{print $1,$3}' | awk 'NR==2{d1=$1;t1=$2} END{d2=$2;t2=$2; print "Rata - rata serangan adalah sebanyak "(NR-1)/(t2+24*(d2-d1)-t1+1)" request per jam"}' > $filerata2
```
Script di atas digunakan untuk menghitung rata - rata request per jam, kemudian hasil dari hitungan dimasukkan ke filerata2 yang nanti output akan ditampilkan di ratarata.txt.

#### 4. Menghapus file temp_date_time
```bash
rm temp_date_time
```

#### 5. Menampilkan IP yang paling banyak mengakses
```bash
cat log_website_daffainfo.log | awk -F'"' '{print $2}' | awk '{a[$1]++} END{for(n in a) print a[n], n}' | awk 'BEGIN{max=0} $1 > max{max=$1} END{print "IP yang paling banyak mengakses server : "$2" sebanyak "max" request"}' > $fileresult
```
Script di atas adalah untuk menampilkan IP yang paling banyak mengakses server. Hasil outputnya akan dimasukkan ke result.txt.

#### 6. Mencari IP yang menggunakan curl sebaga user-agent
```bash
cat log_website_daffainfo.log | grep "curl" | awk END{print "Ada "NR" request yang menggunakan curl sebagai user-agent"}' >> $fileresult
```
Script di atas akan mencari ada berapa banyak request yang menggunakan curl sebagai user-agent.

#### 7. Menampilkan IP yang melakukan request ke server di jam 2 pagi pada tanggal 22
```bash
cat log_website_daffainfo.log | grep "curl" | awk -F'"' '{print $2,$4}' | awk -F":" '{print $1,$2}' | awk '{printf "%s Jam %d ", $1, $3; if($3 >= 1 && $3 < 12)print "pagi"; if(%3 >=12 && $3 < 15)print "siang"; if($3 >= 15 && $3 < 18)print "sore"}' >> $fileresult 
```
Script di atas akan menampilkan IP mana saja yang melakukan request ke server jam 2 pagi. 

### Kendala dalam Pengerjaan

Mengalami stuck lama pada saat mengerjakan script untuk mencari rata - rata request per jam. Setelah berhasil pun jawaban masih salah karena hasil bernilai -1.

### Dokumentasi

#### 1. Hasil rata - rata request per jam di ratarata.txt

![dokum_2.1](Dokumentasi/Soal2/ratarata.png)

#### 2. Hasil IP yang paling banyak mengakses dan IP yang menggunakan curl di result.txt

![dokum_2.2](Dokumentasi/Soal2/result.png)

## Soal 3

Soal meminta untuk membuat dua file shell script, `minutes_log.sh` yang akan dijalankan setiap menit dan `aggregate_minutes_to_hourly_log.sh` yang akan dijalankan setiap jam. Dua shell script ini berguna untuk melakukan monitoring pada sumber daya komputer. Script `minutes_log.sh` akan menjalankan perintah `free -m` untuk memeriksa ram komputer dan perintah `du -sh <target_path>` untuk memeriksa ukuran sebuah directory komputer (dalam hal ini, directory `/home/{user}/`) dan menghasilkan sebuah file log dengan nama `metrics_{YmdHms}.log` yang akan menyimpan keluaran dari dua perintah tersebut. Script `aggregate_minutes_to_hourly_log.sh` akan melakukan agregasi berupa nilai minimal, maksimal, dan rata-rata terhadap kumpulan file log pada jam tertentu dan menyimpan hasilnya pada file log dengan nama `metrics_agg_{YmdH}.log`. Selain itu, file-file log yang dihasilkan akan disimpan pada directory `/home/{user}/log` dan memiliki ijin read-only hanya bagi user yang membuat file-file log tersebut.

### Pengerjaan Soal

#### 1. Membuat file `minutes_log.sh` dan `aggregate_minutes_to_hourly_log.sh`

```bash
touch minutes_log.sh
touch aggregate_minutes_to_hourly_log.sh
```

#### 2. Membuka file `minutes_log.sh`

```bash
nano minutes_log.sh
```

#### 3. Membuat variabel untuk format file log

```bash
prefix="/home/azzura13/log/metrics_"
postfix=`date +%Y%m%d%H%M%S.log`
```

#### 4. Menjalankan `free -m`, mengambil data pada baris memori dengan grep dan awk, dan memasukkannya ke variabel array mData

```bash
mem=`free -m | grep Mem: | awk '{print $2,$3,$4,$5,$6,$7}'`
mData=($mem)
```

#### 5. Menjalankan `free -m`, mengambil data pada baris swap dengan grep dan awk, dan memasukkannya ke variabel array sData

```bash
swap=`free -m | grep Swap: | awk '{print $2,$3,$4}'`
sData=($swap)
```

#### 6. Mendefinisikan path user

```bash
path="/home/azzura13/"
```

#### 7. Menjalankan du -sh $path dan mengambil argumen pertama (path size) dengan awk

```bash
pSize=`du -sh $path | awk '{print $1}'`
```

#### 8. Mencetak setiap atribut dan memasukkannya ke file log

```bash
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $prefix$postfix
```

#### 9. Mencetak isi setiap variabel dan memasukkannya ke file log

```bash
echo "${mData[0]},${mData[1]},${mData[2]},${mData[3]},${mData[4]},${mData[5]},${sData[0]},${sData[1]},${sData[2]},$path,$pSize" >> $prefix$postfix
```

#### 10. Mengubah permission file dan menyimpan `minutes_log.sh`

```bash
echo `chmod 400 $prefix$postfix`
```
#### 11. Membuka file `aggregate_minutes_to_hourly_log.sh`

```bash
nano aggregate_minutes_to_hourly_log.sh
```

#### 12. Membuat fungsi untuk mencari nilai rata-rata dari kumpulan nilai

```bash
avg_result=0

average(){
 local sum=0
 
 for i in echo $*
 do
   let sum=$sum+$i
 done
 
 avg_result=`expr $sum / $#`
}
```

#### 13. Membuat variabel untuk format file log

```bash
first="metrics_agg_"
date=`date +%Y%m%d%H`
last=".log"
```

#### 14. Mengambil data pada file-file log pada jam file script dijalankan

Setiap nilai pada setiap atribut akan dimasukkan ke variabelnya masing-masing. Awk digunakan untuk mengambil baris data yang memiliki kata `/home/` dan dipisahkan dengan koma serta argumen yang sesuai dimasukkan ke variabelnya masing-masing (argumen 1 untuk mem_total, argumen 2 untuk mem_used, dst.). Nilai yang diperoleh pada setiap variabel dipisahkan oleh spasi.

```bash
path="/home/azzura13/"

mem_total=`awk -F "," '/home/ {print $1}' /home/azzura13/log/metrics_$date*`
mem_used=`awk -F "," '/home/ {print $2}' /home/azzura13/log/metrics_$date*`
mem_free=`awk -F "," '/home/ {print $3}' /home/azzura13/log/metrics_$date*`
mem_shared=`awk -F "," '/home/ {print $4}' /home/azzura13/log/metrics_$date*`
mem_buff=`awk -F "," '/home/ {print $5}' /home/azzura13/log/metrics_$date*`
mem_available=`awk -F "," '/home/ {print $6}' /home/azzura13/log/metrics_$date*`

swap_total=`awk -F "," '/home/ {print $7}' /home/azzura13/log/metrics_$date*`
swap_used=`awk -F "," '/home/ {print $8}' /home/azzura13/log/metrics_$date*`
swap_free=`awk -F "," '/home/ {print $9}' /home/azzura13/log/metrics_$date*`

path_size=`awk -F "," '/home/ {print $11}' /home/azzura13/log/metrics_$date*`
```

#### 15. Melakukan agregasi data

Agregasi yang akan dilakukan adalah nilai minimal, maksimal, dan rata-rata. Nilai minimal dicari dengan mengurutkan data secara ascending dan mengambil nilai pertamanya (menggunakan `head -1`) dan nilai maksimal juga dicari dengan mengurutkan data secara ascending tetapi mengambil nilai terakhir dari kumpulan nilai (menggunakan `tail -1`). Selanjutnya, nilai rata-rata dicari dengan menggunakan fungsi average yang telah dibuat sebelumnya. Masing-masing nilai yang diperoleh akan dimasukkan ke masing-masing variabel yang sesuai.
  
```bash
#minimum

mem_total_min=`echo $mem_total | tr " " "\n" | sort -g | head -1`
mem_used_min=`echo $mem_used | tr " " "\n" | sort -g | head -1`
mem_free_min=`echo $mem_free | tr " " "\n" | sort -g | head -1`
mem_shared_min=`echo $mem_shared | tr " " "\n" | sort -g | head -1`
mem_buff_min=`echo $mem_buff | tr " " "\n" | sort -g | head -1`
mem_available_min=`echo $mem_available | tr " " "\n" | sort -g | head -1`

swap_total_min=`echo $swap_total | tr " " "\n" | sort -g | head -1`
swap_used_min=`echo $swap_used | tr " " "\n" | sort -g | head -1`
swap_free_min=`echo $swap_free | tr " " "\n" | sort -g | head -1`

path_size_min=`echo $path_size | tr " " "\n" | sort -g | head -1`

#maximum

mem_total_max=`echo $mem_total | tr " " "\n" | sort -g | tail -1`
mem_used_max=`echo $mem_used | tr " " "\n" | sort -g | tail -1`
mem_free_max=`echo $mem_free | tr " " "\n" | sort -g | tail -1`
mem_shared_max=`echo $mem_shared | tr " " "\n" | sort -g | tail -1`
mem_buff_max=`echo $mem_buff | tr " " "\n" | sort -g | tail -1`
mem_available_max=`echo $mem_available | tr " " "\n" | sort -g | tail -1`

swap_total_max=`echo $swap_total | tr " " "\n" | sort -g | tail -1`
swap_used_max=`echo $swap_used | tr " " "\n" | sort -g | tail -1`
swap_free_max=`echo $swap_free | tr " " "\n" | sort -g | tail -1`

path_size_max=`echo $path_size | tr " " "\n" | sort -g | tail -1`

#average

average $mem_total
mem_total_avg=$avg_result
average $mem_used
mem_used_avg=$avg_result
average $mem_free
mem_free_avg=$avg_result
average $mem_shared
mem_shared_avg=$avg_result
average $mem_buff
mem_buff_avg=$avg_result
average $mem_available
mem_available_avg=$avg_result

average $swap_total
swap_total_avg=$avg_result
average $swap_used
swap_used_avg=$avg_result
average $swap_free
swap_free_avg=$avg_result

path_size_avg=`echo $path_size | tr " " "\n" | head -1`
```

#### 16. Mencetak setiap atribut dan memasukkannya ke file log

```bash
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/azzura13/log/$first$date$last
```

#### 17. Mencetak isi setiap variabel dan memasukkannya ke file log

```bash
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$path,$path_size_min" >> /home/azzura13/log/$first$date$last
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$path,$path_size_max" >> /home/azzura13/log/$first$date$last
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$path,$path_size_avg" >> /home/azzura13/log/$first$date$last
```

#### 18. Mengubah permission file dan menyimpan `aggregate_minutes_to_hourly_log.sh`

```bash
echo `chmod 400 /home/azzura13/log/$first$date$last`
```

#### 19. Menjalankan kedua script dengan crontab

```bash
crontab -e

* * * * * /home/azzura13/soal3/minutes_log.sh
59 * * * * /home/azzura13/soal3/aggregate_minutes_to_hourly_log.sh
```

#### 20. Melihat hasil pada folder log

Ketika kedua script sudah dijalankan, akan muncul file-file log di dalam folder log. Hasil dari script `minutes_log.sh` akan muncul setiap menit dan hasil dari script `aggregate_minutes_to_hourly_log.sh` akan muncul setiap 59 menit/ 1 jam.

### Kendala dalam Pengerjaan

Saat pengerjaan soal 3, terdapat kesulitan dalam mengerjakan script `aggregate_minutes_to_hourly_log.sh`. Kesulitannya yang ditemukan adalah ketika mencari cara untuk mendapatkan data yang dipisahkan dengan koma pada satu file dan dipisahkan dengan spasi pada beberapa file log pada suatu jam tertentu. Pada akhirnya, masalah tersebut dapat diatasi dengan menggunakan perintah awk yang ditambahkan beberapa atribut, seperti `awk -F "," '/home/ {print $1}' /home/azzura13/log/metrics_$date*` untuk mendapatkan data mem_total. Atribut `-F ","` berarti mengambil data yang dipisahkan dengan koma dan `{print $1}` berarti mengambil data pada argumen 1, yaitu mem_total, pada setiap file log.

### Dokumentasi

#### 1. Isi folder log

![dokum_3.1](Dokumentasi/Soal3/Dokum_modul1_isi_folder_log.png)

#### 2. Isi file log

![dokum_3.2](Dokumentasi/Soal3/Dokum_modul1_isi_file_log_1.png)

![dokum_3.3](Dokumentasi/Soal3/Dokum_modul1_isi_file_log_2.png)

#### 3. Isi crontab

![dokum_3.4](Dokumentasi/Soal3/Dokum_modul1_crontab.png)
